Rails.application.routes.draw do
  require 'admin_constraint'
  mount Ckeditor::Engine => '/ckeditor', constraints: AdminConstraint.new
  root 'home#index'
  resources :profiles
  
  match '/about'              =>        'home#about',              via: [:get],            :as => 'about'
  match '/join-our-team'              =>        'home#join',              via: [:get],            :as => 'join'
  match '/privacy-policy'              =>        'home#privacy_policy',              via: [:get],            :as => 'privacy_policy'
  match "/contacts"              =>        "contacts#create",              via: [:post]
  match "/applicants"            =>        "applicants#create",            via: [:post]

  match '/inflight-catering'              =>        'industries#inflight_catering',              via: [:get],            :as => 'inflight_catering'
  match '/industrial-catering'              =>        'industries#industrial_catering',              via: [:get],            :as => 'industrial_catering'
  match '/concessions'              =>        'industries#concessions',              via: [:get],            :as => 'concessions'
  match '/restaurants'              =>        'industries#restaurants',              via: [:get],            :as => 'restaurants'
  match '/retail'              =>        'industries#retail',              via: [:get],            :as => 'retail'
  match '/oil-gas-mining'              =>        'industries#oil_gas_mining',              via: [:get],            :as => 'oil_gas_mining'
  match '/commercial-airline-catering'              =>        'industries#commercial_airline_catering',              via: [:get],            :as => 'commercial_airline_catering'
  match '/private-inflight-catering'              =>        'industries#private_inflight_catering',              via: [:get],            :as => 'private_inflight_catering'


  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end

  #########################################
  #==locations url for location model
  #########################################
    # get '/locations', to: 'locations#index', as: :locations
    post 'locations', to: 'locations#create'
    get '/locations/new', to: 'locations#new', as: :new_location
    get 'locations/:title_for_slug/edit', to: 'locations#edit', as: 'edit_location'
    get 'locations/:title_for_slug', to: 'locations#show', as: 'location'
    patch 'locations/:title_for_slug', to: 'locations#update'
    put 'locations/:title_for_slug', to: 'locations#update'
    delete 'locations/:title_for_slug', to: 'locations#destroy'

  #########################################
  #==news url for blog model
  #########################################
    get '/news', to: 'blogs#index', as: :blogs
    post 'news', to: 'blogs#create'
    get '/news/new', to: 'blogs#new', as: :new_blog
    get 'news/:title_for_slug/edit', to: 'blogs#edit', as: 'edit_blog'
    get 'news/:title_for_slug', to: 'blogs#show', as: 'blog'
    patch 'news/:title_for_slug', to: 'blogs#update'
    put 'news/:title_for_slug', to: 'blogs#update'
    delete 'news/:title_for_slug', to: 'blogs#destroy'

    get '/sitemap.xml', to: redirect("https://s3-us-west-2.amazonaws.com/gcg-catering/sitemaps/sitemap.xml.gz", status: 301)

    match "*path", to: "home#catch_all", via: :all
end
