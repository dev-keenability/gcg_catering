class ApplicantMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(applicant, name)
    @applicant = applicant
    myfilename = name
    attachments["#{@applicant.full_name}_resume.pdf"] = File.read("#{Rails.root}/tmp/uploaded_resume/#{myfilename}")
    @applicant.location.general_contact_email.present? ? mymail = @applicant.location.general_contact_email : mymail = 'lynda.pantoja@goddardcatering.com'
    mail(to: mymail, bcc: 'dev@keenability.com', subject: 'New applicant for GCG Catering')
  end
end
