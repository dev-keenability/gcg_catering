class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact, location)
    @location = location
    @contact = contact
    if @location.present? && @location.general_contact_email.present?
      mail(to: @location.general_contact_email, subject: "New contact for GCG-Catering #{@location.name}")
    else
      mail(to: "stewart.massiah@goddardcatering.com", subject: "New contact for GCG-Catering")
    end
  end
end
