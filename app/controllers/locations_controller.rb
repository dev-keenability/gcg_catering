class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :create, :destroy]

  def show
    # @profiles = Profile.all
    @profiles = @location.profiles.order(position: "asc")
  end

  def new
    @location = Location.new
    @location.build_image
  end

  def edit
  end

  def create
    @location = Location.new(location_params)
    if @location.save
      redirect_to root_path, notice: "Location was created successfully"
    else
      render :new
    end
  end

  def update
    if @location.update(location_params)
      redirect_to location_path(@location.title_for_slug), notice: "Location was successfully updated"
    else
      render :edit
    end
  end

  private

  def set_location
    @location = Location.find_by(title_for_slug: params[:title_for_slug])
  end

  def location_params
    params.require(:location).permit(:name, :title_for_slug, :description, :address, :latitude, :longitude, :title_for_maps, :phone, :fax, :general_contact_email, :specific_location, :iframe, :airport_code, :meta_description, image_attributes: [:id, :pic, :name])
  end

end
