class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :create, :destroy]
  
  respond_to :html, :json

  def index
    @profiles = Profile.all
  end

  def update
    # Commented section is for best_in_place gem
    # @profile.update(profile_params)
    # respond_with @profile

    # We need to use build here, because rails by default will destroy and create an image (has one association) 
    # since its a has one relationship rails's build method does not know how to update(i think), it just deletes and creates a new record
    if profile_params[:image_attributes][:pic].present?
      @profile.build_image
    end

    if @profile.update(profile_params)
      # redirect_to "/location-#{profile_params[:location].downcase}", notice: 'Profile was successfully updated.'
      redirect_to location_path(@profile.location.title_for_slug)
    else
      render :edit
    end

  end

  def new
    @profile = Profile.new
    @profile.build_image
  end

  def edit
  end



  def create
    @profile = Profile.new(profile_params)
    if @profile.save
      # redirect_to "/location-#{profile_params[:location].downcase}", notice: 'Profile was successfully created.'
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    @profile.destroy
    redirect_to :back, notice: 'Profile was successfully destroyed.'
  end

  private

    def set_profile
      @profile = Profile.find(params[:id])
    end

    def profile_params
      params.require(:profile).permit(:full_name, :title, :telephone, :mobile, :email, :location, :position, :location_id, image_attributes: [:id, :pic, :name])
    end

end