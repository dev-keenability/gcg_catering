class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :load_locations

  def load_locations
    @locations = Location.order(:name)
  end
  
end
