class HomeController < ApplicationController
  def index
    @blogs = Blog.order(pubdate: "desc").limit(3)
  end

  def about
  end

  def join
  end

  def privacy_policy
  end

  def catch_all
    @blogs = Blog.order(pubdate: "desc").limit(3)
    flash.now[:warning] = "The page or request you were looking for was not found."
    render :index
  end
end
