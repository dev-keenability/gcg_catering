class IndustriesController < ApplicationController
  def inflight_catering
  end

  def industrial_catering
  end

  def concessions
  end

  def restaurants
  end

  def retail
  end

  def oil_gas_mining
  end

  def commercial_airline_catering
  end

  def private_inflight_catering
  end
end