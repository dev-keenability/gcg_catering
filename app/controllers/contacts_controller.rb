class ContactsController < ApplicationController
  def create
    if params[:thislocation].present?
      @location = Location.find(params[:thislocation])
    else
      @location = ""
    end
    @contact = Contact.new(contact_params)

    if @contact.save
      # Tell the contactMailer to send a welcome email after save
      ContactMailer.welcome_email(@contact, @location).deliver_later
      flash[:success] = 'Thank you for contacting us.'
      redirect_to root_path(anchor: 'maincontent')
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :message)
  end
end
