class Location::Asset < Image

  validate :check_dimensions

  def check_dimensions
    temp_file = pic.queued_for_write[:original]
    unless temp_file.nil?
      dimensions = Paperclip::Geometry.from_file(temp_file)
      width = dimensions.width
      height = dimensions.height
      
      if width < 1500 && height < 750
        errors[''] << " dimensions are too small. For a good quality background please upload a larger image. Minimum width: 1500px, minimum height: 750px"
      end
    end
  end
  
  ##============================================================##
  ## Paperclip Interpolates
  ##============================================================##
  Paperclip.interpolates :imageable_type do |attachment, style|
    attachment.instance.imageable_type.downcase
  end

  Paperclip.interpolates :imageable_id do |attachment, style|
    attachment.instance.imageable_id
  end

  has_attached_file :pic,
    :path => "#{Rails.env}/:imageable_type/:imageable_id/:style/:filename",
    :styles => { 
      :big => ["1500x750>", :jpg]
    },
    default_url: "//s3-us-west-2.amazonaws.com/gcg-catering/hero/hero-locations.jpg",
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
     :big => "-quality 80"
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }



  validates_attachment :pic, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true

end


#needed default_url here because in the new action the build method semi-creates an image, and needs a default image otherwise it would say missing