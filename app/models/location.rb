class Location < ApplicationRecord
  has_many :applicants

  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  
  has_many :profiles
  before_save :title_url_it
  has_one :image, as: :imageable, :class_name => "Location::Asset", dependent: :destroy
  accepts_nested_attributes_for :image, update_only: true #otherwise it will delete and then reinsert everytime you reach the edit page, instead of updating it
  # accepts_nested_attributes_for :image, allow_destroy: true # use above for has one relationship

  def title_url_it
    self.title_for_slug = name.downcase.squish.parameterize("-")
  end
end
