class Image < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true
  delegate :url, :to => :pic #This is for dynamic styling, everything is now being delegated to Profile::Asset < Image && Blog::Asset < Image
end