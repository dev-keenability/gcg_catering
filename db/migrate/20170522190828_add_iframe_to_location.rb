class AddIframeToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :iframe, :text
  end
end
