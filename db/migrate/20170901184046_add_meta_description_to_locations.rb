class AddMetaDescriptionToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :meta_description, :string
  end
end
