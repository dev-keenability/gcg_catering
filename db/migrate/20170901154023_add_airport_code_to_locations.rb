class AddAirportCodeToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :airport_code, :string
  end
end
