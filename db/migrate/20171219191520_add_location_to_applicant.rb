class AddLocationToApplicant < ActiveRecord::Migration[5.0]
  def change
    add_column :applicants, :location_id, :integer
  end
end
