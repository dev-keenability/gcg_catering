PGDMP     ;                	    u            d70s48r9s2pu0m    9.6.2    9.6.0 K    3           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            4           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            5           1262    8039865    d70s48r9s2pu0m    DATABASE     �   CREATE DATABASE "d70s48r9s2pu0m" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
     DROP DATABASE "d70s48r9s2pu0m";
             rabvjxqlglvxpl    false                        2615    2200    public    SCHEMA        CREATE SCHEMA "public";
    DROP SCHEMA "public";
             rabvjxqlglvxpl    false            6           0    0    SCHEMA "public"    COMMENT     8   COMMENT ON SCHEMA "public" IS 'standard public schema';
                  rabvjxqlglvxpl    false    7                        3079    13277    plpgsql 	   EXTENSION     C   CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
    DROP EXTENSION "plpgsql";
                  false            7           0    0    EXTENSION "plpgsql"    COMMENT     B   COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
                       false    1            �            1259    8307705 
   applicants    TABLE       CREATE TABLE "applicants" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 "   DROP TABLE "public"."applicants";
       public         rabvjxqlglvxpl    false    7            �            1259    8307703    applicants_id_seq    SEQUENCE     u   CREATE SEQUENCE "applicants_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE "public"."applicants_id_seq";
       public       rabvjxqlglvxpl    false    202    7            8           0    0    applicants_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "applicants_id_seq" OWNED BY "applicants"."id";
            public       rabvjxqlglvxpl    false    201            �            1259    8042469    ar_internal_metadata    TABLE     �   CREATE TABLE "ar_internal_metadata" (
    "key" character varying NOT NULL,
    "value" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 ,   DROP TABLE "public"."ar_internal_metadata";
       public         rabvjxqlglvxpl    false    7            �            1259    8042554    blogs    TABLE     �  CREATE TABLE "blogs" (
    "id" integer NOT NULL,
    "title" character varying,
    "content" "text",
    "content_index" "text",
    "title_for_slug" character varying,
    "main_image" character varying,
    "pubdate" timestamp without time zone,
    "meta_description" character varying,
    "meta_keywords" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."blogs";
       public         rabvjxqlglvxpl    false    7            �            1259    8042552    blogs_id_seq    SEQUENCE     p   CREATE SEQUENCE "blogs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."blogs_id_seq";
       public       rabvjxqlglvxpl    false    7    200            9           0    0    blogs_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "blogs_id_seq" OWNED BY "blogs"."id";
            public       rabvjxqlglvxpl    false    199            �            1259    8042541    ckeditor_assets    TABLE     �  CREATE TABLE "ckeditor_assets" (
    "id" integer NOT NULL,
    "data_file_name" character varying NOT NULL,
    "data_content_type" character varying,
    "data_file_size" integer,
    "assetable_id" integer,
    "assetable_type" character varying(30),
    "type" character varying(30),
    "width" integer,
    "height" integer,
    "created_at" timestamp without time zone,
    "updated_at" timestamp without time zone
);
 '   DROP TABLE "public"."ckeditor_assets";
       public         rabvjxqlglvxpl    false    7            �            1259    8042539    ckeditor_assets_id_seq    SEQUENCE     z   CREATE SEQUENCE "ckeditor_assets_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE "public"."ckeditor_assets_id_seq";
       public       rabvjxqlglvxpl    false    198    7            :           0    0    ckeditor_assets_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE "ckeditor_assets_id_seq" OWNED BY "ckeditor_assets"."id";
            public       rabvjxqlglvxpl    false    197            �            1259    8042490    contacts    TABLE       CREATE TABLE "contacts" (
    "id" integer NOT NULL,
    "name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "message" "text",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
     DROP TABLE "public"."contacts";
       public         rabvjxqlglvxpl    false    7            �            1259    8042488    contacts_id_seq    SEQUENCE     s   CREATE SEQUENCE "contacts_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."contacts_id_seq";
       public       rabvjxqlglvxpl    false    190    7            ;           0    0    contacts_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "contacts_id_seq" OWNED BY "contacts"."id";
            public       rabvjxqlglvxpl    false    189            �            1259    8042528    images    TABLE     �  CREATE TABLE "images" (
    "id" integer NOT NULL,
    "imageable_id" integer,
    "imageable_type" character varying,
    "name" character varying,
    "pic_file_name" character varying,
    "pic_content_type" character varying,
    "pic_file_size" integer,
    "pic_updated_at" timestamp without time zone,
    "primary" boolean,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."images";
       public         rabvjxqlglvxpl    false    7            �            1259    8042526    images_id_seq    SEQUENCE     q   CREATE SEQUENCE "images_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE "public"."images_id_seq";
       public       rabvjxqlglvxpl    false    196    7            <           0    0    images_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE "images_id_seq" OWNED BY "images"."id";
            public       rabvjxqlglvxpl    false    195            �            1259    8042479 	   locations    TABLE     �  CREATE TABLE "locations" (
    "id" integer NOT NULL,
    "name" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "title_for_slug" character varying,
    "description" "text",
    "phone" character varying,
    "fax" character varying,
    "specific_location" character varying,
    "iframe" "text",
    "address" character varying,
    "latitude" double precision,
    "longitude" double precision,
    "title_for_maps" character varying,
    "airport_code" character varying,
    "meta_description" character varying,
    "general_contact_email" character varying
);
 !   DROP TABLE "public"."locations";
       public         rabvjxqlglvxpl    false    7            �            1259    8042477    locations_id_seq    SEQUENCE     t   CREATE SEQUENCE "locations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE "public"."locations_id_seq";
       public       rabvjxqlglvxpl    false    188    7            =           0    0    locations_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE "locations_id_seq" OWNED BY "locations"."id";
            public       rabvjxqlglvxpl    false    187            �            1259    8042517    profiles    TABLE     |  CREATE TABLE "profiles" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "title" character varying,
    "telephone" character varying,
    "mobile" character varying,
    "email" character varying,
    "location_id" integer,
    "position" integer,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
     DROP TABLE "public"."profiles";
       public         rabvjxqlglvxpl    false    7            �            1259    8042515    profiles_id_seq    SEQUENCE     s   CREATE SEQUENCE "profiles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."profiles_id_seq";
       public       rabvjxqlglvxpl    false    7    194            >           0    0    profiles_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "profiles_id_seq" OWNED BY "profiles"."id";
            public       rabvjxqlglvxpl    false    193            �            1259    8042461    schema_migrations    TABLE     O   CREATE TABLE "schema_migrations" (
    "version" character varying NOT NULL
);
 )   DROP TABLE "public"."schema_migrations";
       public         rabvjxqlglvxpl    false    7            �            1259    8042501    users    TABLE     �  CREATE TABLE "users" (
    "id" integer NOT NULL,
    "email" character varying DEFAULT ''::character varying NOT NULL,
    "encrypted_password" character varying DEFAULT ''::character varying NOT NULL,
    "reset_password_token" character varying,
    "reset_password_sent_at" timestamp without time zone,
    "remember_created_at" timestamp without time zone,
    "sign_in_count" integer DEFAULT 0 NOT NULL,
    "current_sign_in_at" timestamp without time zone,
    "last_sign_in_at" timestamp without time zone,
    "current_sign_in_ip" "inet",
    "last_sign_in_ip" "inet",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "admin" boolean
);
    DROP TABLE "public"."users";
       public         rabvjxqlglvxpl    false    7            �            1259    8042499    users_id_seq    SEQUENCE     p   CREATE SEQUENCE "users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."users_id_seq";
       public       rabvjxqlglvxpl    false    7    192            ?           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "users_id_seq" OWNED BY "users"."id";
            public       rabvjxqlglvxpl    false    191            �           2604    8307708    applicants id    DEFAULT     j   ALTER TABLE ONLY "applicants" ALTER COLUMN "id" SET DEFAULT "nextval"('"applicants_id_seq"'::"regclass");
 B   ALTER TABLE "public"."applicants" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    201    202    202            �           2604    8042557    blogs id    DEFAULT     `   ALTER TABLE ONLY "blogs" ALTER COLUMN "id" SET DEFAULT "nextval"('"blogs_id_seq"'::"regclass");
 =   ALTER TABLE "public"."blogs" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    199    200    200            �           2604    8042544    ckeditor_assets id    DEFAULT     t   ALTER TABLE ONLY "ckeditor_assets" ALTER COLUMN "id" SET DEFAULT "nextval"('"ckeditor_assets_id_seq"'::"regclass");
 G   ALTER TABLE "public"."ckeditor_assets" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    198    197    198            �           2604    8042493    contacts id    DEFAULT     f   ALTER TABLE ONLY "contacts" ALTER COLUMN "id" SET DEFAULT "nextval"('"contacts_id_seq"'::"regclass");
 @   ALTER TABLE "public"."contacts" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    190    189    190            �           2604    8042531 	   images id    DEFAULT     b   ALTER TABLE ONLY "images" ALTER COLUMN "id" SET DEFAULT "nextval"('"images_id_seq"'::"regclass");
 >   ALTER TABLE "public"."images" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    195    196    196            �           2604    8042482    locations id    DEFAULT     h   ALTER TABLE ONLY "locations" ALTER COLUMN "id" SET DEFAULT "nextval"('"locations_id_seq"'::"regclass");
 A   ALTER TABLE "public"."locations" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    187    188    188            �           2604    8042520    profiles id    DEFAULT     f   ALTER TABLE ONLY "profiles" ALTER COLUMN "id" SET DEFAULT "nextval"('"profiles_id_seq"'::"regclass");
 @   ALTER TABLE "public"."profiles" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    193    194    194            �           2604    8042504    users id    DEFAULT     `   ALTER TABLE ONLY "users" ALTER COLUMN "id" SET DEFAULT "nextval"('"users_id_seq"'::"regclass");
 =   ALTER TABLE "public"."users" ALTER COLUMN "id" DROP DEFAULT;
       public       rabvjxqlglvxpl    false    192    191    192            0          0    8307705 
   applicants 
   TABLE DATA               g   COPY "applicants" ("id", "full_name", "email", "phone_number", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    202            @           0    0    applicants_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"applicants_id_seq"', 1, true);
            public       rabvjxqlglvxpl    false    201                       0    8042469    ar_internal_metadata 
   TABLE DATA               U   COPY "ar_internal_metadata" ("key", "value", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    186            .          0    8042554    blogs 
   TABLE DATA               �   COPY "blogs" ("id", "title", "content", "content_index", "title_for_slug", "main_image", "pubdate", "meta_description", "meta_keywords", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    200            A           0    0    blogs_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"blogs_id_seq"', 3, true);
            public       rabvjxqlglvxpl    false    199            ,          0    8042541    ckeditor_assets 
   TABLE DATA               �   COPY "ckeditor_assets" ("id", "data_file_name", "data_content_type", "data_file_size", "assetable_id", "assetable_type", "type", "width", "height", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    198            B           0    0    ckeditor_assets_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('"ckeditor_assets_id_seq"', 1, false);
            public       rabvjxqlglvxpl    false    197            $          0    8042490    contacts 
   TABLE DATA               k   COPY "contacts" ("id", "name", "email", "phone_number", "message", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    190            C           0    0    contacts_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('"contacts_id_seq"', 1, true);
            public       rabvjxqlglvxpl    false    189            *          0    8042528    images 
   TABLE DATA               �   COPY "images" ("id", "imageable_id", "imageable_type", "name", "pic_file_name", "pic_content_type", "pic_file_size", "pic_updated_at", "primary", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    196            D           0    0    images_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('"images_id_seq"', 199, true);
            public       rabvjxqlglvxpl    false    195            "          0    8042479 	   locations 
   TABLE DATA                 COPY "locations" ("id", "name", "created_at", "updated_at", "title_for_slug", "description", "phone", "fax", "specific_location", "iframe", "address", "latitude", "longitude", "title_for_maps", "airport_code", "meta_description", "general_contact_email") FROM stdin;
    public       rabvjxqlglvxpl    false    188            E           0    0    locations_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"locations_id_seq"', 23, true);
            public       rabvjxqlglvxpl    false    187            (          0    8042517    profiles 
   TABLE DATA               �   COPY "profiles" ("id", "full_name", "title", "telephone", "mobile", "email", "location_id", "position", "created_at", "updated_at") FROM stdin;
    public       rabvjxqlglvxpl    false    194            F           0    0    profiles_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"profiles_id_seq"', 95, true);
            public       rabvjxqlglvxpl    false    193                      0    8042461    schema_migrations 
   TABLE DATA               1   COPY "schema_migrations" ("version") FROM stdin;
    public       rabvjxqlglvxpl    false    185            &          0    8042501    users 
   TABLE DATA                 COPY "users" ("id", "email", "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at", "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip", "created_at", "updated_at", "admin") FROM stdin;
    public       rabvjxqlglvxpl    false    192            G           0    0    users_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"users_id_seq"', 1, true);
            public       rabvjxqlglvxpl    false    191            �           2606    8307713    applicants applicants_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "applicants"
    ADD CONSTRAINT "applicants_pkey" PRIMARY KEY ("id");
 J   ALTER TABLE ONLY "public"."applicants" DROP CONSTRAINT "applicants_pkey";
       public         rabvjxqlglvxpl    false    202    202            �           2606    8042476 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ar_internal_metadata"
    ADD CONSTRAINT "ar_internal_metadata_pkey" PRIMARY KEY ("key");
 ^   ALTER TABLE ONLY "public"."ar_internal_metadata" DROP CONSTRAINT "ar_internal_metadata_pkey";
       public         rabvjxqlglvxpl    false    186    186            �           2606    8042562    blogs blogs_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "blogs"
    ADD CONSTRAINT "blogs_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."blogs" DROP CONSTRAINT "blogs_pkey";
       public         rabvjxqlglvxpl    false    200    200            �           2606    8042549 $   ckeditor_assets ckeditor_assets_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "ckeditor_assets"
    ADD CONSTRAINT "ckeditor_assets_pkey" PRIMARY KEY ("id");
 T   ALTER TABLE ONLY "public"."ckeditor_assets" DROP CONSTRAINT "ckeditor_assets_pkey";
       public         rabvjxqlglvxpl    false    198    198            �           2606    8042498    contacts contacts_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "contacts"
    ADD CONSTRAINT "contacts_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."contacts" DROP CONSTRAINT "contacts_pkey";
       public         rabvjxqlglvxpl    false    190    190            �           2606    8042536    images images_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY "images"
    ADD CONSTRAINT "images_pkey" PRIMARY KEY ("id");
 B   ALTER TABLE ONLY "public"."images" DROP CONSTRAINT "images_pkey";
       public         rabvjxqlglvxpl    false    196    196            �           2606    8042487    locations locations_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY "locations"
    ADD CONSTRAINT "locations_pkey" PRIMARY KEY ("id");
 H   ALTER TABLE ONLY "public"."locations" DROP CONSTRAINT "locations_pkey";
       public         rabvjxqlglvxpl    false    188    188            �           2606    8042525    profiles profiles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "profiles"
    ADD CONSTRAINT "profiles_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."profiles" DROP CONSTRAINT "profiles_pkey";
       public         rabvjxqlglvxpl    false    194    194            �           2606    8042468 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY "schema_migrations"
    ADD CONSTRAINT "schema_migrations_pkey" PRIMARY KEY ("version");
 X   ALTER TABLE ONLY "public"."schema_migrations" DROP CONSTRAINT "schema_migrations_pkey";
       public         rabvjxqlglvxpl    false    185    185            �           2606    8042512    users users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."users" DROP CONSTRAINT "users_pkey";
       public         rabvjxqlglvxpl    false    192    192            �           1259    8042551    idx_ckeditor_assetable    INDEX     m   CREATE INDEX "idx_ckeditor_assetable" ON "ckeditor_assets" USING "btree" ("assetable_type", "assetable_id");
 .   DROP INDEX "public"."idx_ckeditor_assetable";
       public         rabvjxqlglvxpl    false    198    198            �           1259    8042550    idx_ckeditor_assetable_type    INDEX     z   CREATE INDEX "idx_ckeditor_assetable_type" ON "ckeditor_assets" USING "btree" ("assetable_type", "type", "assetable_id");
 3   DROP INDEX "public"."idx_ckeditor_assetable_type";
       public         rabvjxqlglvxpl    false    198    198    198            �           1259    8042537 /   index_images_on_imageable_id_and_imageable_type    INDEX     }   CREATE INDEX "index_images_on_imageable_id_and_imageable_type" ON "images" USING "btree" ("imageable_id", "imageable_type");
 G   DROP INDEX "public"."index_images_on_imageable_id_and_imageable_type";
       public         rabvjxqlglvxpl    false    196    196            �           1259    8042538 !   index_locations_on_title_for_slug    INDEX     i   CREATE UNIQUE INDEX "index_locations_on_title_for_slug" ON "locations" USING "btree" ("title_for_slug");
 9   DROP INDEX "public"."index_locations_on_title_for_slug";
       public         rabvjxqlglvxpl    false    188            �           1259    8042513    index_users_on_email    INDEX     O   CREATE UNIQUE INDEX "index_users_on_email" ON "users" USING "btree" ("email");
 ,   DROP INDEX "public"."index_users_on_email";
       public         rabvjxqlglvxpl    false    192            �           1259    8042514 #   index_users_on_reset_password_token    INDEX     m   CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" USING "btree" ("reset_password_token");
 ;   DROP INDEX "public"."index_users_on_reset_password_token";
       public         rabvjxqlglvxpl    false    192            0   U   x�3��H-��.UHN,I-��K�L�I�p�NM�KL���,��K���442�4204�50�52U04�24�25�3067�4�#����� g�          A   x�K�+�,���M�+�,(�O)M.����4204�50�52Q0��21�21�372537�#����� �H      .   �  x��X�N�H�OQ��Nv���Bsi!��H�@�vşj��]C��T�i:Oó�d��r�en���(��:��|��V���.��p�.da��B:��=i�,�ek&e���q���J�{+�ulo��N���n���S�g������1^�L��3Az�u�ҝ!�4YLء�Uٚ[�bSa3"ѓB~���#�t�ފ��I36pY��o2ul\��� *�#���K2Q��6l*������ܧ��;���$���0�r�\��g9W��{���D�Y�c1��O;#�ڻ��X~>11;s�;��JgN��2��AI��j_^K-��J�x��/ةH~���"H ����Ã\x�:/y1��;�]Lw�AQ�������Е��%����Q�!�J�D���]��Bk*��F�`�r��6E��ʰ��N��ؘQ����^salt���K��n32��p�N Ж��3W����i��YگL����u1k='��7u+�F|�c��)ҡ��S6����� �(�D�`�z]ACS�.�jQ�j�l�2`�6�V�d�����E�PvDR9D������W�\�P�9�tf+�(g��qd��<��W7�o=H�B���\�DԒ��T���G�Ԁ�x�]�H�����qx�����\���o����U�n_�����}������;7*�>W����W�U�\��nO��x�k3i�j:F$��i+���S��ڽ,v�pwg��=�p�CE��,~{�o嫻�o઻���SKYy���n�Ln���^���p�N�3�N��8j�L�,�,BdlT�XD<@#��W�`L4���|"YDT�+�V����~X����ȉ�u������`{}3��6����W�����v#�nlv뫽����|.@�k����n�'�B�X9� C���(wIș��y�sn����'�p��tj�5ȰjY�{~f%�+P)`#���5�V� 0�d�k�r ���-R�)$:��-;XS�R����+�Tl�fu2Wx��L2����{6�%���F�����ɓ�uYI������!]�w�l�U�/R��\�k��V��V�,1BP���t��;�fq�sR�!G���e��"1zJ�p�o��Ҥ�{��z�2��e#N����*�zc�^�ة�3b�:����<��)�}�;!jU\YMb3��C���Ѹ�65R�9��`75��d� ���_��@����_����t��#H���t�9G���0/�b{�VB�U��֣^�����[�no���b��������7�ݕ7�G,b�{�����|x��<y����B\Q���yo�UO�O���X�OxQ�y���ෘ�'�!ϥ�lO]�_[�Y0��,�9��xh�_����L�x|�͈�@:�L�=%~F1W�r��7Ԧ�4	���f�fb\�0ث`.ŵ7 ��V�6���˜��z�~���r�h�D09�#�0�� ��\hs�3��6�%�0�,0[���cf��A��-�0�`�������C�ӟq�>�짡c4��z�FM� 
lF�fn_*Y��Tb�vp"L�M �Yh��B�㤕���2�u�:��l�4�M#?v�&�+�٩��o������}��{�M�cGq����IlU"�0����&��&O���D��������rmɕ(�JQ�bn�C�<g��*,6�_P~a��ch@�T��a�5���/�����I���-V��>�~��N3�C@�4"���W����<�p˰c��ӑ����FpC�6ӕJi�xlM��f��/�)&iO��qҬUU�PT�����P|�_>�DRP��d����M��C�4��g�+�}�Q�r�6�7�����i�<i�H�2p��e�$�W�����\�b�0ܵ��}�.F��ѐ�? EO��R�l�����(�+q#G
�Ӿׁ1�",t�p�5�Řތ��U�,�^h�S"嶶��i���(�a�� �K�"����u �A�R�M��i:��3|�2���- dx�*_�n�CQ��9���hﳥ<`�t2mh��۶` �fz�[)��`b֨y'l��_xS[����O��5*�zh���J�MĻ)]Z"���#���Ӣ�f��������Ko�^z��Լ����(D/=�W��'A���|{��/��������`4�dѕ��sx�z?>YZ뷶{[q�����.�����x�����X��WWW��B�I      ,      x������ � �      $   c   x�3�,I-.Q�L�I�p�NM�KL���,��K��Kq�dd+ Q�XeZQ~�BAQ~JirIf~��������������������������9)�=... �w"      *      x��\Ir�H�]��]W-��;�35���u��
Bd(���*jUiu��V��O��� |��S�B�'�?�����q�
狷}�i�i���.�7�纽��/>�.ַ�u󟟿4�WLhW��gg����R��p楕�HP��(�2�R�x��i��;�2:&_|i4�o�[>���P?��n������!B9�V��T��#������{��0�^K�f�R���nU��]K�<뮮���L���+�#�T�������dKaVx���)j�c:9��2��t��뫺�O�~�^�\�`R�xH�b��-�T.���'\&P�=ÒO���v�|���Ú�Tgճ�m�zS��[0��d��9���b
�dv��N��{@��B��x���^����V�[��R=�G����_�����q-!��R.5_f\J���17�Xu�b����(iy֞ho�'���J.2{�Ʌ�65�	�E�"�o�n��4Ϻ���>�W��zw���	Ae�;����B$��0�T�����@ (a��?�a`ϰm�o��}�/��]wf�M�u�j�A�[���K�=᠘��uFH���*��
��i]~3����hO�Yj���g��$�,����<@X+UX7]���~�������R�e_�{)�gV��}��`�A�%�dD�BJi\l�S��0�0����Q甋c��K�����,�z!�v&�]SKE�8]�����z�m6�m�xV]�׫\�����{K��y�}�@�{��3H��clSS�mZڻG���m���ͧOd�������uL�x*�d|��o}����b��g���(��#D��������x>^]o����j���]��w�Jp̣�\�ς#�h<��)f����㗦m��M����E�?�d�����=�"Q��pz�|��<�i���6I�Ɖi�GB����U��!�ۄ�*����mU�!�%;�<�f�dbXQ$R��$�'Q�dr!�d�e�>�~,!`*S׫볃R9�R�����K�WKsTA�Lp2B����p�V�ۏjM!
�X�yP/4�K
����5(\�O\J8�aD��A�-�B ��pn�(���y�A���S�����L��x����?��㈊-�_ y�[`�<��郂��2ܗʒ�N�<���54�i6�Q�a��&�eҦl�����&����M
�"������h�R�+�F�؀%m�[g��>�"�.FH�S&0�U}rYSЬ񐊢���J�Rђ�������:.�A)�
Q9�e��u�W3�Y<�;�0iD�1�l�DD�O��A0:a�(�y(w�������IcRCdţ�"�Y�:Р���&1<HL�I	s���vSC�_��zwڙ������$:��t� $���W����J5��)�W]���=��N��)�l���4�6�_�-��vEm�yN-��0^�Y�����~��	yF��92�R��r]&� ��5��4\^P�(l:���U]C44�)2Ny&t4����Z���61� ��l���T,��S%���;-^oQ�tPU�n5���+ܙD^�C�E�pT$YLA�i�53X@����vwӵw�e}�o6i	�)�����!�ir�LM�+DQ20�g�r�ޡ��S�K��0�6C>�:n�p�0oq�6rH�����?��bK��Π7�ba�*����#Tz>郱d$�L�W�������#
}�Wj*Q��fu������[Y������I�(��G�>@c�!a��;��,8$�T"�<���G|�.o�]u�YLV����S��z�*2n�E�L�� ���r�T	��T�~HʠX͵Ng&�@�Z�㙧P	?~p��Dy`�Y< ')
�	����T�^3o�*Q�	�t`^v�z���$�9n�1��\jJ�!v�i�|@5U�޺�ĳ2�C/����w��|)
Z�c������ �B���W)�{�h*�OC%P)��޵���z��Wc�1a���FC�l��;���=�p�ZM��C\Ƞ&~܁��j���/*ٟ�ۺ�*�T+}��`����SD83I���A8���QQ쵎�`
�"/t�j$)A㶫��n�M1�
�/�W�$S1�#�P��@`���l^����LM��d mc�:���09�RF���o�[8��}?4?��v�LXd�N@�i%��Yf�D~�!0�T�MCE}s�5z|�,�0ʪd��ևe�e�n�P��,�>������z�}�����O䌰q@��F.�S6� 4Y�&ll�jx�5�]�~��t����籒��*�㩺�&u�#(*7������^_ÊW?5��u�Dp���FT�t��|feF�N^a
��/xt>��}�m��U��K�c(�m�Ɗ�DbotƒGT�E�kS��t�tb/w}� ��jz��_�=����֩Dt0BB���:�N�;@`�����}���{���:��`��,���P��j2\������e�i�]�7� �����.�P�ҩf�d�Zg$73��"v�ns�V�Ȋ���f�Pm�J̩,B=�9B ��/2=O�k���m���9!��j�q<1& cx�ȈZn���@D&؜�u��4wջ��/��:�"�5RQ�;*D6;A���V|S�`{�u�ɳ�Ǐ`��Ru���>� I�.�UJ��ڈJf�S3PI�$����M�yѶ��6M��x@>D3ds+3L�31���.��rD�������DDP�����ia*��1��+!��x��:��٬�7��z흍��	}��b&e0�v�b;�B%Gc����n�oۘ��R��X��q�e�bD��aB`� /,�}<�IwT3��A�3��D�zZ�$PI}���.w��l��
ۛ8�<�4x�P�A��+/!j�P�"�}w[WO����-aℏW��T�� d��u�3��W �mS��O��;���̒�%�cK�� Z��?7B`������r����VP���`��v��Bj��1�!��l��PA�G��Tu���Su��K�V���r��Z~2��Qs=��-lxt�|D�yS=�0�A�9{�S�������S=�tm���z����Ôt�)�����Q3���\�z��!U�w��kd��"������`B�����f ����e�� {/��/)��ۦ���+T0����H��Y<]���3�Z�33�\�"��_��k���=΍V:|[K�x:5�ؽd�����|*�xx�����fNv$���'"�z�
*=�V�NYcR%�:�:�LҁYIn�?�\֛��U�Ϝ�*������K�'N9JZ���Ⱦ(q)�D7���O�"���XgS���X�de�u�L��� ] �t"H�X���x����6�[��KQI`<��C|ሓt��a��X �)�{� ��N�Ǵ�5����~E{���1�e:-�B��5��\h7��i|-F��`;\�y���Xv��#!�*MǳX�3��vɑ���\�@��P1����]�qX̴�'��,�fh�/ .��F�>� 7O���[<�ګ}���x�����;ʩ�W�(4(7b*����+�����Y��]��'�(���TM��-~~�:{������œ�/_�����o޿��/��|���_��W������z�����O���rܵu:&χ	8�6%χc���v*���I����I��I����'�^̓'�a��s�:�{ԐF��A��̝F��b�ի7�_\�W�/^~8?���V�;��Wt��K� �=��㷟�J��&�D�7���~���PJ�C�=��`C���:ͧlP�<�H����F:(O���V�ݳ�DQR3/�QO�K�F�s��dda�B�"��.w2�IwRAn�`=,>ja�촌��ԽPr*R�Ta�����~H�ou��rw���X�ΐ�*e�4v�6�l�B%����{��W-�É��7z���'��y��uU���?8B%]q��ڙ�n���&��0E�77��@ cD��]��^�}�2ʁ�gՓ������Z���Kj'�3�d����� �  �a�I�J�$�=x������!�N#��n��)��&�P���{@�B���J��D~;�6t�yv��<�Pґp4�������A��~�pas�n��~������O��VKa��XN0j,��[k@���J:N%��,���휀aJ�;;޾���(�QC�kz�)�@K2(Q/�C������oη�=mz3u��Z�'����\GD�7����/���3ȪB�������z��w�߱�(E�Jg"����<E��b�g<@��v_�u����n��h(X$�X	����du��T/�=Mg7�)�	,�D�'&�ԨgR�P{����j]��ra��|
r?��ȯ�?bx �֒:ޕ�B`�$��5A�Q��>�AZL&�;|.���T'��.���c��O�t�J���\g8��mP���;>Z�VC[Iur��;�w3P��R��Hx[<|Ew^�����ئG[A�E�9��Z�g���CG�$^כ;:�:~���%SR��|E���J���خ!�d�1;�tkN�ہ�f�n�k��W݉�B9�f��,�q<Zdޓ�:���R�@�*g��9���\c{VM�3Z�"'���
��nѽ����~��C@Q.��] �r^�G�C���}s�L�G��#�Of����R�d(���?���A��A���|���=7g����9"��yG�{�/�i1�t�T����{�w�J�<O�M_�0��K��J��f }7��>�a�Ox��I8|�6T����E�%"�����Cw��G7۟6�f=����*���'�}!�,�jɌ#��˸�3P�����{]����=]i�y��B(�=��_J���Q1;���u:��/T���7��[�g��hѨ?j�Ɔ
�f�P1�j��D�{{sN������?D� ��<b諑��r�E!%X��AYx&�~}<-��v��&�]4ͬ%
Kͨ�p[�J�DJ�P�`Bk�{� ��*�L�>��G��a�����D�yBj�x:=���ZY�ᩩ:e��,6�)����_��pso������m��W���Ut6�D��3&�`ܧ�����=x�Jj%�ivX���yq_uV,�_7?���c��Af 0�tP7��H������[z76CM*�l<���>��j��6��_���#T�mQ���Uu�      "      x��}�vܶ��u�)��JN�JE� 4��.Ɏ�D���t�:7(J�qUQ!Y�ի/�5�!���<I��,�~dY�r��"E�������]dcs���j@���":�Q�b�5�~�ݯ�IXF:�����qpdJ�����Wd�6�[E�\Ll�W6X����z1�����l^'�$.�$�_��]��	����2���r���6-���,ɂ��N�"�˽�,&��5�dp�.��s�^^����6)-��J;s�*���������)�lL�<H��pb�36�I�mԕ)���� ]�e�.lQ����W^�E@epg�Ƚ�W�]�1yj˻ �&�gP4H�E�͡�{P_2[N�������U0�����2�ٖ�Sve���tR�A �u��v�����WC0ͳ9�`��H,����GG�K�Ԁx�&Ia0�S�q�+��^�Ў��Z���X;���f�,K��%�T�$w���Ha/��A�� ���/D�h�lc��<�]�n���~_��N�9N�"O~���,���������2�.g6�~���u�o�c;����/������:ᚊPI��H
�_�� "a,��(�()�|BiH���`R�dD_0�bJ^0�����M)a�KU�S�C
��9�]A>Ɖ�c>�&LD
����|"E4�ZJŕ��V��(���B��G�慴Ջ
����qC����8�qĿ�	<)�~���������^�WN`�,����k�5������s 7`���r6+�f�����>.���&�q�ɔ�*�t��b5�\�/.���� �6v�n��O-�ڿ��J�Fe��� �622r@��%"$Sk�(�d!��K�+�������UՋ��p;����UX�a�E}�g7�$U,Af)�ˤ���2��A+��� �6� �?�f�ؽ��"E�\A�K;+�{�����Ln�1^��f��=(:�o��v�P��0��\�Ic��D�)�kl>���0i�oP����6�5{�z�ͬ��
@\�_f7�Es��`�	�7��2����Dw	���W�3hpK�����ޙs��X�����(F NFuܾdZ��A"	N����j76׸>[
��p�4��4#A"�D�i�^�Q����� �3��@c%�8f��b�H�$6�"1&��BJ��:� �ƊF�k1�+1�_�Eگ���n�����
��:~.�����|�FѨ�%�I�H�]L���_�9:ݍ-�x�p�p/`�u�p+
T�B��+���UH�V_\esS�1L��!lH�aRR����/��U_oB��s���Q�<pA:Pg�(q���٠t[@Q\��/�,��pu�����"P�x�6b�-�%�o�Fw���
R70���]�l�p��I� j_L޸ ��@jJ��wt����U�"��.���H��W\��Tġ��R��:Q�;"�����D$~�$Zp�M���DM�r�����5n�G�"���h�N�6VĲ���nF��6��ؿx=�xȚ�1n�׫�5���<�	ŷ�u��sf��{�//��n8(�*�� ���= q(��[V�;������Q�	���u����A?a�:~0Ǚr���j�lU���8+���s4��Y�P�����ҬW�<��3�i%m����P��g'͝��v���t1��SX��.��LR�;^:Xlo�(O@%4�K���(��=�5\�
��[�j��ݶ�a*���#;OA�M�`�v��7�e��֝�����]�t����Yo@�# N@{?�������`�	N�?����)u<`�*�\]߈{�0Ll������ KXYP���]�Y淄j>?�⩐+��*��TF!a��
U���*h��(�MDB��r*%��	v�_/��q�-��U_��T�`�?�-����a�4�0�4������p*�9��S�R����Ok�tN��_�ޝ��3��Sج��rl֔Vƚh�������8 �qe;�l�t�Uݲ�9كj��v��L0�W�]FwA�tp_�=���z"��M&�$V@�š�R�Z��~D�k��6ok��K,ֆ*�i��~�^gr@҂���z:�na�oiy�����&�Q{��E
�B�yp���t�&:xr�+�Y��6�8��<��}x�j+�@c>`B��ůK�Ck@���V����v��Y�s���$�k�e:#�C�l��}���J��*� ��N�P #�������WKO�8�4���C%�t,E��-QS��k�b���;�Wӧ��{@38Z?h>`�zNhTjsM?{^�T높jVa�����aXTX��b�ʛ @+�ˎ�@����[`��m1x�d�C���$+��S��B��򀈐IEXo\�t������Y��>�f��Z��_<�8"�a	�1����1��>��kk:����Ξ2���Ѣ2T�$��6Q=�F�7�]�˾O�K�&MV� �\�f��eه/ƲƁ�p&��`�@Q�*����;Fi��]0����jn����^pt(^�?�<��k�i�,�BN#A��p�؜��2-�px�CP)�9���c�|*&�t2�ر�3Jb�@�4cP��n�W������q����U,��V�y�8{��Ik`PV��DD5��='�Xs�imy�+X8<~�qx���g-8P����a����t�A��֑,>�2��PѺ^��q(a"ݛ��u��g37���#�����&z��L���uѝ7�^yd/���@�8x�l�1�[�!��,.�I
*���@^G�<���uC���'���zu�tG�b�4n�:z�xl[̱�k�iр[���Ҥ3L�y����c�X�ّ��]#��!��}��j��(H��`���@K6�͓Xs����ߟ�qd�<�@#�4���1AjE,��`D{â$�E��]�%�H&81�P�Ɠ��zG�$p���Z'�1�����&�͒>`~bHj)qQc��yq
��0n�1������_~m��O��b�<9~8�Խ�l���iKyf���8�!��o3��y6��.�����b����V�H�-H����6ͅ��������� bӁ�5_jeh��­p�* tiH�ћ���;��;������-v�R�a�0��q5?@��&̝�۴ځ��Y1[B;�潅1D~pg�QaX/��h�hP}� z�9>�����]&��l8(��^�zt0k�=8��u�2X|��I�S�p.�wd�,�܅=��@iUJ��E�V���W�g(nm��}�x��@H%�Uзꊐ�8b�R*Н#�H t�0`c①˧:A�%�8"ʫ�����S*̔C�ūY�%��I�?Y���|$���>��|�38CD�����_-w����nw��10c!�`���؉P*(�}EZ˞���=-��u��:����ó?'l9dV=<I�.d f~ ��a$9��j� �b�˸�T�;�jF�0��+/���"+��Ժ�C�R���D��a�Z������6Xv��"�iW�E���hA�ke&l��֑g��~L��K}һ��,:P�c��X#��1��?��1��͍@J:���]��`@.��mO�&�}i˿�?PQ�`��d؅�ϥ�=(FZ�nS�*V�9�UH��(��T� ��<��͗�@E�3�f��&�N�!@�o�����Y
j\ɦ�J8��D`��'��5dN�J��	�ѽJܱj,��6�rt4��?}�A��e^-�[׈C�;^�;��2�ut{z@��a��nS�'*�LDR�.��Çi�5���x�LӚ��>N� ����ew�V$u���m��]@K?����x��4K;�L��2�C<�?UY��\㗄>���z 4���7#�2-��sVX����fs;ɞ�ư��R�j��t �t�1w*:�`GG�4�SM�A�8�J~
2�T4�0X�����������������[	������0��J:B���rt�������?^�^�y�W'5�;��l���1��A\�|7�u����k�8%��\�0n��    ���q���U�>��Y0?���������@ ��Lj�K�3�p���00N��m�n�J(��F|a�N�k�_���mƆ�8v���B},�{� j.�@�NhY]�Z�����tfѻ	�	�3�s>؋t�L*!7®g�� �=LK	�#k��4� G�H��.C�:d����0Ni��b�4��N�$6�᠐�1Z�nn�I�>�����l��uW85��}�/H�T?b���;L�=/4k�=����c� �ul����v��������U��8���\�Ywm}�V�����x�,�t=� Qb(@�fJ
�')�I=s�7�B]��-t��c��W�B
��Z���s��M�sm�ز׃�*P����[�������<ܫ|n16�.����{#e�긋$6��_�BA��i�aPwp�;G�do�"�s�}�)�t͕�3"�2YvQ,�=� *`㵋ěa��n�r�J׹�6>Ԥ��p�NJ�����C�9�Е��?K�0�?��ώi�_�k��P�b@���'N�b95	&���Ew}�����w�̽>2��x�_�
n���$����_��]�s%��-*�����|��PP��w���D�׼��n������|Ec��n���P�,Q ��.�d<�]f�.� b$�uf�_���+*�a��1TAU9�Z�J�1�2>����ÆA ����O�$S�$�T��Q��;:2:a|�a�jz�ٞ6���=׮�k(*�(I�A(GlC��n�����qr~�ڛ�TPm���N�2��-(܀�U�<��z�6�/'f͒��)�a"Ԧ)��P�H�7�Jwm���C�j��L悠�ة7�� �W ���5x;�̠��p\�7���FT�n#���pcgXKkk,�,�M��ҝAW��*g��������E��������%���� xh.�`��lyq�c�����>�Y~�'�	��%�B4F*�$%�J=R�D�Hi��H�9C��0"���Ǳso���L�0\M�XjLL:	����wq�×���8�ƒ���'&	;��m���I��6���_���ˏ���}Op(��־������@ �z�7��2ltqܻ�KwzW׹�������9"�\���tht
�̀!b��Ѱ�~�·kf�Q6�5U[�����uS��y�ZZT���G��q?K}VS*&jÌ�����7�\��M��m�A�nV�s���_������>E��&lk�,@��vaNR��G��9U��A}N���&����Sc����z���p_�����E��|��n�,8�e�)Ҟ�� �a�f��aj/������x8z���ƧVkX�xeVm��vVη�׍����;R����f�@�b��\k;�:Fq�?�xf4��l���幊Z�Uo�^�u�p�4qi�+x��0�Z{#�t�޻K����{�k������ǖ�t�ae�)w���{�5c��\cӖ��N��C��O5�ypj�3]�T���ȾAC���C�D�x�$�ƙ]L�(�u4|�VlFC<�	�
9�9��L2u�}�i��2ڽx�)T�#q��܀<0B�m>+��O7���b�L>~"~�cL{�1�y�HC��Z�9E�>�\�`�Ԁ<>:�)2�d��q��J2���H9��)�l%����J��ђy_F�4E��0��Ȟ�U��V���(g��+ϖh�n������|���-��NzGK��:�n�nz�B��[�8	a�����t7��ބ�C�)��ݟH�$���Ӈ�}Q�@.��
�V����pKl}���)��}�S�O4����AiXB����W�6j�DBO�c��hFID;W���Y1�5E5v_tHO��x5����*! �!�Q.��Ʊ��g0>e��$J3�hAÆsĿ*��RJ�޽�O)�$�|���J��Hc[��4*�5�#ɛ����ţ���S+����>��f>{���W�p���L�|o�S~b���F��M)�a��lP�5t:۪:�T���$���B�1���ʜ�Ӽ;>��������7i �ޝބ��(��%�9�ο���W��֦9�M��f��F�?�Ç��9>���V��5g��i�Rn��S��T�Z~WYq���ǤWC�4q�_�:>`@�BR]q=@�8*p��7w=��s��+I("�	L&
@�\��cLpL�DxׅYQ�r)�W�8�I,�2��!/�`��� |<r���f���;���HL���l��ߢ*�;n	pX+?�K���(W���p�W���tH� 7�쟎N��S���'���6C�C�Hp���������K�׫S9�YT��%J�l�a�`�T��tSwpԉO]��.����Y'�|K��Fv�ږ1����Һhk���3'ݙS��'g{�EnP�tމ�lX[ j�*J;3p��"\���V���}�O�{20��r־������|S�t�Dq��6E��4'&���x��E���"�c���XS.�ph�#�SN��Š�m�.J�	e�hk�dّA�:̤o�����k��Q&�6��Lq���x3���U�#�&� �HS!P��Hp~�c͏�h5?�jp��{?β<���� �`D��*����1��3VQoZ�]�]u�'k&�_�9�ݪ^� ��֯����A���u��{[��O$h׭B��Tl^��ϓ6/+(j.���a3�p�ܻ��(&�li�n�<<�o��4�*3���M����9YQ�tj��g��Zg��\�uc�=�s(=OK�؝���.��Y3�1C�M&�wa8�M�g��Nh�u��B�nih��7��}�k�Z�P����\]Ȉ������|l�⮶ʍ�x~3�x��5�U�P�u��������ՠ��rd>�H��� tJ�)�?��Sp( @�_�[3����VLD��h��=rR�w�dT0�l>��L%��@�Gz�P#�jr&�2~<b�z����[^�X�����mxn��.�@���Y����
b��xc"��� K�C�H0���]�N�]u.3fϡ��w�7@ux�����ő+����e:�^�\�(�U���vx��4.��9��s��ǲ�k[�� ���hr٥���tT�&[��O'>K�%��?����<lK
o����c����&L3��4�Fs���7>���-��_e��8bs���
���9C������sh����O9�!)��%�\��`���,�M�dH�g�e����@7�fw�Bq(�sE2r�m`��$�uH�Q�G�8�\Ӊ��`jS ��;]�-�)��1�)k0J�F���2E�����+��&�\����p2!�	�89tYƵ���x��l����m��`���ZG��g3^�����vX��]@�`��V�*4�c��7�,�J�~V֥�E��v������͂�V�/�ߎ`0��pL/���6aٶ�-�|,[zϰ+���P��`)�t�B9�H��9�	$zfa¬�T�ۃf<���C������ �/nc�邧<��H���߂����	�\:�\�(��joHp�1��mbA��w�i^C��G-��k ��.`ƕ�� u���/*��m����EvS�y'nWZ"��(�G���>=x��D��Q�\p <��������JP� ;�y�'�;FC1M(�"�\�\P��gj��u�800�ɒ,��q1�u���� _�c^���g\�w9{��F�}\6z�}\᳇�K+�+�1seΝ��N�WP=ܿ�8?�Ӣ�s���qn1ye���8�� +�kfE��	��a��]Veג��͵��V���*���z�E�U?��0���,��d:I�{m�\M��}t�N��/o
#W�J�� �n~��aT<�ٌ`O��!)�_bb��f�	 ݥ���H*B�a��tW��#c���{#!fQs�1�r��������[�?�ȗ�:9���O���R�Y��C�j�DW�kA���=‫ �x�5%��V"��D�5��U#Ԍ#M#o
�����������P���cˉ�lgh�����@6�t� (  ������l���K묡��]1�݇���f��So͛S�<fF3�C�l�Z>�t��pʄĕpUG�	���}��ӄ��V��I\�]����[�o�~x��/&8���X<dpn'y��3�q�F�c��[�+VO��>�7�09�n����Xj@�q� �]�(�;Q`�-TGlV(V��\;��x�q����#���Zl����߽;}_���A�O��}:��?k�Ekr۬~����y5#����T	Yǹ�e��8s}������=�#E�چ�ҋ�>L�q�Z�+�0��2�M��&�G�ο�eP�+���-3��g�6�?���M
[d�����ŕ�&c�7�S����^_�(�-�j��xn�\�z�fx�����$x;<={�}r���08={;<y5z��!C�_>>Ʌp�J�U���>T�B
��K�i��cƄQ���&2PhLu�L�	�9�su��~��훍$���q�����:C�w�3�Âm���-����R=/٘�1�Ux�
�G���ӟNN�࿂Ϝ+�M�5�ꫯ���H�      (     x��ZKs�8�>��Ǚ��x��i]v=����[��ؘ-�6�(�KJ�o���G헠d�|��7b#|�2�L __&�؛���v�$��U���}Qm^%��:�-ZfC�PNȅ4�2v�gO"��6�5�_�ۢ-��t�l�b�)!�B����K-�2�ZX��	7�sޮ�.9�6y�n:��{��mˇ"9�+nc�Ȑ�{��P�Z�R�P�Hܱ�m�{�nq��+�;v�]�.�-�,��!�6y�}�*-�z�tSD�I}��B��Pi�	3 �4"uN;�T�S�U�uru_�+�����*o���^³�2�-���d��*.K�~�A����c����*��^)�'J.�^��L�y�~|���p0ɮ�b�����Ԗ�g�Wgt]�R���O�fn����4(�~Q�I�Bjmf���T�u��K~�k6�������y�}4�R�y
��]�hErY�9;[��]���m2yR�ؚ�S�>sL7�F�I�����ĥ���A������1pc�6qŌF���_ 62����t�Z,Gf��X���*4����n�|-�f׮���G�yg�GiMz߯���d�L"3c�^|L�ұ�������5�~��t�XLg17v�����]��ĥG�6}Z=��6�Vy��ַe]�1K�._�U�-'�' Q7��|�z�	=*Zerh�c�R�C�%o���Ɖ6ba�Yx��@�czM�����Lj��ntSG$.�,�Ur�|�H.����&�u��'$~��¿�jJ,�%�H���'H\	�i���h����f�M�m�O��
�t�'�HV���R��+km8A�
��w���n�}*�V؟�؅�!C�]���3z�0��.U�f��r�H����5�cr�ovp�q��=C�E�I��	�Z
��r6��ĕf}�N>���
�O���cZ�gd�v�~g��LȠ�v?&qe�[��k�T)��:�tX/�8�X�/ �!�I��X#e�qT_�P�#
WȢE]��y�W�ϗ{�-��;2DE<�j��T0� �c��$���8\Ǩ�V ���ۥ����	�@9&q���M]��䪬P�֛�.�-����o"1��d٠�(� qIȳD�n��^�!a
��8��aK8�Dz>�O�#��!l��li,�ux���03
@Ke����MQ�����Z/����������D�BSǊ��8A�*��;�ȧ��:(��$/�I�,iE,���H>@�թȴ�����k�zUv��lZ ��ҋ�-Vp�����@NǕ�֤�~�ܽ�`�R��clv������>�x��ϻ-b������|?�2�N��*<�h#���j�ss��u���'� ���1d�*�m�ޭ���>�w����l�&pQ-���H���i��l�T~W�d
x﫺�G�ǜ�с�)��S�k:"qm�ŵ�=��Y�.Z*����P��LӚ��"@�4�5c��8�]�eQQ���(� �uF��69�˼[ ��ͦ첎�������5�i�ՠ��8��y���ɻ��.&)�nca$�-��I�0�Ċ�7q�nC���q�:;L8�$�3v�S��U�#��]^Qg�w����M��-]��������
����N����,ڶH.��3uQhK��T`�>��N'=lt
��)7bo���
��S9�Q�H<�i���E�9u�P�^Z�
����������`BAbщTGo�)7�}AiN>�շ�qAS���S`t�l�}��r����w���Z�Oy�Y\��t�,�)��$�v�l�Ӽg��d�a)=zi�H�#7�yr~�w��0Ơ� 4	Y�*�z�tE�Ӻ�c��Dj���f�S�X�H�8���n���f܌S9B\ߤ-Q�E��h@V4R�P��?A�Ƴ�E��_$���OÐ��N�E&��m���`Z�>��)���=A��I�$n2�aw۠�ʇ�)@	`5�cwX��<��SKd[�T�7�q���	Ͻ �o �Q�à��iz�o�[Mk��k�JѰH+3��[�.ʼ.����������q�@J�u��2TWZ�~ۯ�Op�4SC�f�-�V�g�#�����y[ŉ6l���98t�g��%�t��OM&��'H�ky�\�Z�MQ����'P�/��&=�����5$�_3���H� C���9A�V�����`�$�ۦ�h�H)������zK�s=�J2N&��#e=MW�ͬ���#�m^=��D5� �8sad!P9���z��:b�SGej�T��癶��9j��5���Gϓ/O���tG藰�����i�4�{mG�tTQ#�P�	G�߻����ޭ�R'�l��r!�GD�^�Es*��\��JɑF/�=�����/;`u7*t(�{DEZ� T�t�3�A�!䇽���l��%��R�T{��@����Gn$ ���wݖ�Z�n�F[���=p�<a߅��Ƃ�ӿ�w�J2��Y�������pj��O�&�����b���vN@�VM@�Xq�R�i��OIB��q��oaqꑑaK�+Ο<��Q��a1��Sq9,�ό�Go%�f���9R�-�w�ǣ�4�����89C��ڵņT{N>Sz��B�4�ĥ3��n-��dH3>�߱�: 7����V��J�#̨LJm���p'��]A���k��F(6aCm2���^E{޴%�9M�@�D�y&�ʜ q��Y����f<���Ӻ�R+9��Y��V({�ĝbo׻���y�z^,z2����$L��=�s�R�p�ĝf�����j�ɦ�NO,�Md��@�Š�C�tv���I�Yv��W�#� }B���ʇ�����M�s���Xm���h�B�������)_�vz��$�;+�"�߾�ZN��!2��h2K��n�D�LΓ���d%���k��xc6���Ђ�/����8��O���ER��|�؛]W�E�%:���8;����P�͂Ϟ�S$���k���;���^�T��悶D�q��6C�J�3'Hܣ�5]�����V��-<`���p ���i��T��,�r����F�A���K�4S0���]�NW�*Z�p̟֮]}�3�F�eK!Pq%�	6���(���&�9�Bs�~(�^�:2�~#q.��$�3v�� ������͇Z,����1�������b�i�C��Ν qc{zy�D[�ah�T�)�Eu��8�2�a�N-���"Cxg�+],�� sD����W��M�NΔ�4��F|fu�Uޞ ��D]�㲦r����7��	<����5����oV��'HS����	Z�v�%sJ�W�}�`�M�8�$���[���4���=��?!*��sR�pk��K����<�g�y�3��e�O���@������(���LR�H��7*��r����[����о:#M^�u������6I�'{ƹ�)X�;�*�Ί�T��>e|�w0Cv�ĳ��Wy�S�4�y/��,�����S�K}�ă@;E�û�(��^G[���4�΋S$$�9�"��/��_?��zk�j��\�	��?A�A����
	Hŉ"b�<MP��Q���H<转��|�9��9���}a7K��	�'H<��D���p{0��oo{P�Ra�s5�$�̡R]�T�1���k�{X
�z����u�M�������u��,m�+�Gp'_�#{�"E�d?/���˓����jSn�^D���pII�:�
��@_&(�-�=�6��)V�dh+�J�9JJ_"P�E!�
�t�/.��Z���������X��׉J)J��>�wN|���\�g��3��/����         e   x�U���0�{��l0	�����G�Z��C6`�B�Q��a/^��/�d�l�\�q�3&V����P)���ލ�y�o�ݣ���y3�_L����]c�?2~-�      &   �   x�m��NAE�=_�b�S��~Tw���\(6�mę3:�=A���nnN�=�^��E����y]���lj��*'��޻�]���C��w�i;�ۏQ9�)�s�w�����e�DU�Ǔ�G����HR�+�{d��=�`�Jә�{��h� ��/����� ���,hl-X���m�`s�3>2�s��U	�QM��,ˎ��=�     